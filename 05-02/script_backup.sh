#!/bin/bash

## Declaro variable FICHERO y le asigno el $1 el cual va a indicar que es un parametro que recibe el script
FICHERO=$1

## Evaluo si se esta pasando un parametro al ejecutarse
if [[ -z $1 ]]; then
        echo "Se debe pasar un parametro"
fi

# Evaluo con "-f" si el fichero existe sino sale del script devolviendo un status code 1
if [ -f $FICHERO ]; then
        echo "El archivo $FICHERO existe"
        echo ""
else
        echo "El fichero no existe"
        echo "Reintente la operación o revise el parametro ingresado"
        exit 1
fi

# Chequeo si el fichero es ejecutable para el usuario
test -x $FICHERO
if [ $? -eq 0 ]; then
        echo "Mostrando informacion del archivo $FICHERO"
        ls -l $FICHERO
        echo ""
        read -p "Desea ejecutar el fichero: [S/N]: " ANS
        if [[ $ANS == "S" ]] || [[ $ANS == "s" ]]; then
                echo "Ejecutando fichero"
                echo ""
                sleep 2s
        else
                echo "No realizare ninguna accion"
                exit 0
        fi
else
        echo "No tiene permisos de ejecución en el archivo: $FICHERO"
        echo ""
        echo "Validando si es propietario del archivo"
        sleep 1
        if [ -O $FICHERO ]; then
                echo "Es propietario del archivo. Podemos cambiar los permisos!"
                read -p "Desea continuar? [S/N]: " ANS1
                if [[ $ANS1 == "S" ]] || [[ $ANS1 == "s" ]]; then
                        chmod 755 $FICHERO
                        sleep 1
                        echo ""
                        echo "Permisos modificados exitosamente, imprimiendo informacion del archivo: $FICHERO"
                        echo ""
                        ls -l $FICHERO
                else
                        echo "No realizamos ninguna accion"
                        exit 0
                fi
        else
                echo ""
                echo "No tiene permisos en el archivo y tampoco es el propietario"
                echo "No se realizara ninguna accion"
                echo "Saliendo..."
                exit 1
        fi
fi
